<?php


class VoximplantAPI {
    private $host = '';
    private $access_token = '';
    private $domain = '';


    protected static $userAgent = 'Swagger-Codegen/1.0.0/php';
    protected static $sslVerify = true;

    public function __construct($host, $domain, $access_token)
    {
        $this->host = $host;
        $this->access_token = $access_token;
        $this->domain = $domain;
    }

    public function runScenario($scenario_id, $phone, $variables, $caller_id)
    {
        // Проверка обязательного параметра 'scenario_id'
        if ($scenario_id === null || (is_array($scenario_id) && count($scenario_id) === 0)) {
            throw new \Exception('Missing the required parameter $scenario_id when calling runScenario');
        }
        // Проверка обязательного параметра 'phone'
        if ($phone === null || (is_array($phone) && count($phone) === 0)) {
            throw new \Exception('Missing the required parameter $phone when calling runScenario');
        }
        // Проверка обязательного параметра 'variables'
        if ($variables === null || (is_array($variables) && count($variables) === 0)) {
            throw new \Exception('Missing the required parameter $variables when calling runScenario');
        }
        // Проверка обязательного параметра 'caller_id'
        if ($caller_id === null || (is_array($caller_id) && count($caller_id) === 0)) {
            throw new \Exception('Missing the required parameter $caller_id when calling runScenario');
        }

        $resourcePath = '/scenario/runScenario';
        $formParams = [];
        $queryParams = [];
        $httpBody = '';

        // form params
        if ($scenario_id !== null) {
            $formParams['scenario_id'] = self::toString($scenario_id);
        }
        // form params
        if ($phone !== null) {
            $formParams['phone'] = self::toString($phone);
        }
        // form params
        if ($variables !== null) {
            $formParams['variables'] = self::toString($variables);
        }
        // form params
        if ($caller_id !== null) {
            $formParams['caller_id'] = self::toString($caller_id);
        }
        // body params
        $_tempBody = null;

        $headers = [
            'Accept'=> 'application/json',
            'Content-Type'=> 'application/x-www-form-urlencoded',
        ];

        if (count($formParams) > 0) {
            if ($headers['Content-Type'] === 'application/json') {
                $httpBody = json_encode($formParams);
            } else {
                // for HTTP post (form)
                $httpBody = http_build_query($formParams);
            }
        }

        // this endpoint requires API key authentication
        $apiKey = $this->access_token;
        if ($apiKey !== null) {
            $queryParams['access_token'] = $apiKey;
        }
        // this endpoint requires API key authentication
        $apiKey = $this->domain;
        if ($apiKey !== null) {
            $queryParams['domain'] = $apiKey;
        }


        $query = http_build_query($queryParams);

        return $this->request('GET', $this->host . $resourcePath . ($query ? "?{$query}" : ''), $headers, $httpBody);
    }

    public static function toString($value)
    {
        if ($value instanceof \DateTime) { // datetime in ISO8601 format
            return $value->format(\DateTime::ATOM);
        } else {
            return $value;
        }
    }

    public function request($method, $url, $headers, $httpBody){

        $retryableErrorCodes = array(
            CURLE_COULDNT_RESOLVE_HOST,
            CURLE_COULDNT_CONNECT,
            CURLE_HTTP_NOT_FOUND,
            CURLE_READ_ERROR,
            CURLE_OPERATION_TIMEOUTED,
            CURLE_HTTP_POST_ERROR,
            CURLE_SSL_CONNECT_ERROR,
        );

        $curlOptions = array(
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLINFO_HEADER_OUT => true,
            CURLOPT_VERBOSE => true,
            CURLOPT_CONNECTTIMEOUT => 65,
            CURLOPT_TIMEOUT => 70,
            CURLOPT_USERAGENT => self::$userAgent,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $httpBody,
            CURLOPT_URL => $url,
        );

        if (!self::$sslVerify) {
            $curlOptions[CURLOPT_SSL_VERIFYPEER] = 0;
            $curlOptions[CURLOPT_SSL_VERIFYHOST] = 0;
        }


        $curl = curl_init();
        curl_setopt_array($curl, $curlOptions);


        $curlResult = curl_exec($curl);
        // handling network I/O errors
        if (false === $curlResult) {
            $curlErrorNumber = curl_errno($curl);
            $errorMsg = sprintf(' cURL error (code %s): %s' . PHP_EOL, $curlErrorNumber, curl_error($curl));
            if (false === in_array($curlErrorNumber, $retryableErrorCodes, true)) {
                curl_close($curl);
                throw new \Exception($errorMsg);
            } else {
                // arning($errorMsg, $this->getErrorContext());
            }
        }
        $requestInfo = curl_getinfo($curl);
        curl_close($curl);


        // handling URI level resource errors
        switch ($requestInfo['http_code']) {
            case 403:
                $errorMsg = sprintf('portal [%s] deleted, query aborted', $this->host);
                throw new \Exception($errorMsg);
                break;

            case 502:
                $errorMsg = sprintf('bad gateway to portal [%s]', $this->host);
                throw new \Exception($errorMsg);
                break;
        }

        // handling server-side API errors: empty response from bitrix24 portal
        if ($curlResult === '') {
            $errorMsg = sprintf('empty response from portal [%s]', $this->host);
            throw new \Exception($errorMsg);
        }

        // handling json_decode errors
        $jsonResult = json_decode($curlResult, true);
        unset($curlResult);
        $jsonErrorCode = json_last_error();
        if (null === $jsonResult && (JSON_ERROR_NONE !== $jsonErrorCode)) {
            /**
             * @todo add function json_last_error_msg()
             */
            $errorMsg = 'fatal error in function json_decode.' . PHP_EOL . 'Error code: ' . $jsonErrorCode . PHP_EOL;
            throw new Exception($errorMsg);
        }

        return $jsonResult;
    }
}



/**
 * Создаем обьект для работы с api
 */
$api = new VoximplantAPI('https://kitapi-eu.voximplant.com/api/v3', 'b24sc1895', 'fe552d03d7004014b7d8b1aa0e06706d');

/**
 * Обязательные параметры
 **/
$scenario_id = 27213; // Номер сценария
$caller_id = 2947; // Номер с которого звонить
$phone = "+79995487574"; // Телефон клиента

/**
 * Переменные для сценария
 */
$variables = [
    'client_name' => 'Вася пупкин'
];


try {
    $result = $api->runScenario($scenario_id, $phone, $variables, $caller_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling: ', $e->getMessage(), PHP_EOL;
}



